import React from 'react';
import PropTypes from 'prop-types';
import {Button} from "@mui/material";
import HighlightOffIcon from '@mui/icons-material/HighlightOff';

const ClearButton = ({onClick}) => {
    return (
        <Button
            onClick={onClick}
            color="secondary"
            variant="text"
            endIcon={<HighlightOffIcon />}
        >clear text</Button>
    )
}

ClearButton.propTypes = {
    onClick: PropTypes.func.isRequired
}

export default ClearButton;
