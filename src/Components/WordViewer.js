import React from 'react';
import PropTypes from 'prop-types';
import {
    OutlinedInput,
    InputAdornment
} from '@mui/material';

const WordViewer = ({word, isValid}) => {

    return (
        <OutlinedInput
            id="outlined-adornment-weight"
            error={!isValid}
            readOnly
            value={word}
            endAdornment={<InputAdornment position="end">{isValid ? 'valid' : 'invalid'}</InputAdornment>}
            aria-describedby="outlined-weight-helper-text"
            inputProps={{
                'aria-label': 'weight',
            }}
        />
    )
}

WordViewer.propTypes = {
    word: PropTypes.string.isRequired,
    isValid: PropTypes.bool,
}

WordViewer.defaultProps = {
    isValid: false
}

export default WordViewer;
