import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Grid} from '@mui/material';
import LetterCard from './LetterCard'
import {chunk, isLetterCloseToAnySelected} from "../../Utils/BoardUtils";

const Board = ({letters, validWords, reset, onChange}) => {

    const [selected, setSelected] = useState([]);
    const [chunks, setChunks] = useState([]);
    const [isValid, setIsValid] = useState(false);

    useEffect(() => {
        const currentChunks = chunk(letters);
        setChunks(currentChunks);
    }, [letters])

    useEffect(() => {
        const word = selected.map(sel => sel.letter).join('');
        const isValidWord = validWords.map(w => w.toUpperCase())
            .includes(word.toUpperCase());

        setIsValid(isValidWord);

        onChange({currentWord: word, isValid: isValidWord, selected})
    }, [ selected ]);

    useEffect(() => {
        if(reset){
            setSelected([]);
        }
    }, [reset])

    const isSelected = (letterCoords) => {
        return selected.find(sel => {
            const {x, y} = sel;
            return x === letterCoords.x && y === letterCoords.y;
        }) !== undefined;
    };

    const handleOnclick = (letterCoords) => {

        if(isSelected(letterCoords)){
            return console.log(`${letterCoords.letter} is already selected`);
        }

        const currentSelection = selected.slice();
        if(currentSelection.length > 0 && !isLetterCloseToAnySelected(letterCoords, currentSelection)){
            return console.log(`${letterCoords.letter} is not close to any selected letter`);
        }

        currentSelection.push(letterCoords);
        setSelected(currentSelection);
    };

    return (
        <Grid container >
            {chunks.map((row, y) => (
                <Grid container spacing={4} key={`row-${y}`}>
                    {row.map((letter, x) => {
                        const letterCoords = { y, x, letter };
                        return (
                            <Grid
                                key={`col-${y}-${x}`}
                                item
                                xs={3}
                            >
                                <LetterCard
                                    onClick={() => handleOnclick(letterCoords)}
                                    letter={letter}
                                    selected={isSelected(letterCoords)}
                                    status={isValid ? 'valid' : 'invalid'}
                                />
                            </Grid>
                        )
                    })}
                </Grid>
            ))}
        </Grid>
    )

}

Board.propTypes = {
    letters: PropTypes.arrayOf(PropTypes.string).isRequired,
    validWords: PropTypes.arrayOf(PropTypes.string).isRequired,
    reset: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
}

Board.defaultProps = {
    reset: true
}

export default Board;
