import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import {Paper} from "@mui/material";
import classNames from "classnames";

const orangeGradient = ['rgba(247,122,38,1)', 'rgba(250,207,90,1)'];
const redGradient = ['rgba(162,6,30,1)', 'rgba(239,75,89,1)'];
const greenGradient = ['rgba(74,154,36,1)', 'rgba(171,229,76,1)'];

const Card = styled(Paper)(({ theme }) => ({
    background: `linear-gradient(0deg, ${orangeGradient[0]} 0%, ${orangeGradient[1]} 100%)`,
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: '#fff',
    fontWeight: 900,
    width: '100%',
    border: '3px solid #f6615f',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 12,
    display: 'flex',
    fontSize: '6vw',
    borderRadius: 12,
    textShadow: '-3px -2px 10px rgba(0,0,0,.4)',
    '&:before': {
        content:"''",
        float: 'left',
        paddingTop: '100%',
    },
    '&.valid': {
        borderColor: `${greenGradient[0]}`,
        background: `linear-gradient(0deg, ${greenGradient[0]} 0%, ${greenGradient[1]} 100%)`
    },
    '&.invalid': {
        borderColor: `${redGradient[0]}`,
        background: `linear-gradient(0deg, ${redGradient[0]} 0%, ${redGradient[1]} 100%)`
    }
}));

const LetterCard = ({letter, selected, status, onClick}) => {
    return <Card onClick={onClick} className={classNames({
                selected,
                valid: selected && status === 'valid',
                invalid: selected && status === 'invalid'
            })}
        >{letter}</Card>;
};

LetterCard.propTypes = {
    letter: PropTypes.string.isRequired,
    selected: PropTypes.bool,
    status: PropTypes.oneOf(['', 'valid', 'invalid']),
    onClick: PropTypes.func
}

LetterCard.defaultProps = {
    selected: false,
    status: '',
    onClick: () => {}
}

export default LetterCard;
