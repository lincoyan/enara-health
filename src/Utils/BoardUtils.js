export const chunk = (items, itemsPerChunk = 4) => {
    return items.reduce((chunks, item, index) => {
        const chunkIndex = Math.floor(index/itemsPerChunk);

        if(!chunks[chunkIndex]) chunks[chunkIndex] = [];

        chunks[chunkIndex].push(item);
        return chunks;
    }, [])
};

export const isLetterCloseTo = (letterCoords, targetCoords) => {
    const {
        y: letterRow,
        x: letterCol,
        letter,
    } = letterCoords;

    if(letterRow === undefined || letterCol === undefined){
        throw new Error('Invalid letter');
    }

    const rowBoundaries = [
        (letterRow - 1),
        letterRow,
        (letterRow + 1),
    ];

    const columnBoundaries = [
        (letterCol -1),
        letterCol,
        (letterCol + 1),
    ];

    const {
        y: targetRow,
        x: targetCol,
        letter: target,
    } = targetCoords;

    if(targetRow === undefined || targetCol === undefined){
        throw new Error('Invalid target letter');
    }

    const areClose = rowBoundaries.includes(targetRow)
        && columnBoundaries.includes(targetCol);

    console.log(`is ${letter} close to ${target}: `, areClose);

    return areClose;

}

export const isLetterCloseToAnySelected = (letter, selected) => {
    for(const target of selected){
        if(isLetterCloseTo(letter, target)){
            return true;
        }
    }
    return false;
}
