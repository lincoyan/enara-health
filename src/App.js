import './Assets/fonts';
import "./Assets/Styles/App.css";
import {Grid} from "@mui/material";
import Board from "./Components/Board/Board";
import ClearButton from "./Components/ClearButton";
import {useEffect, useState} from "react";
import WordViewer from "./Components/WordViewer";

import boardLetters from './Data/test-board-2';
import dictionary from './Data/dictionary';

function App() {

    const [isWordValid, setIsWordValid] = useState(false);
    const [word, setWord] = useState('');
    const [reset, setReset] = useState(true);

    const [letters, setLetters] = useState([]);
    const [validWords, setValidWords] = useState([]);

    useEffect(() => {
        setLetters(boardLetters.board);
        setValidWords(dictionary.words);
    }, []);

    const handleChange = ({currentWord, isValid}) => {
        console.log('word has changed: ', currentWord, isValid);
        setReset(false);
        setWord(currentWord);
        setIsWordValid(isValid);
    };

    const handleClear = () => {
        setReset(true);
    }

    return (
        <Grid container spacing={1} padding={2} >
            <Grid item xs={12} lg={10} padding={3}>
                <Grid container>
                    <Grid  item xs={12} display={{ xs: 'block', md: 'none' }} height={50}>
                        { !!word && <ClearButton onClick={handleClear}/> }
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Board
                            reset={reset}
                            validWords={validWords}
                            letters={letters}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12} md={5} ml={5} padding={2}>
                        <Grid container>
                            <Grid  item xs={12} display={{ xs: 'none', md: 'block' }} height={50}>
                                { !!word && <ClearButton onClick={handleClear}/> }
                            </Grid>
                        </Grid>
                        <Grid container alignItems="flex-end">
                            <Grid item xs={12}>
                                <WordViewer word={word} isValid={isWordValid}/>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default App;
